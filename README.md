![pipeline status](https://gitlab.com/vdshop/public/docker-images/gitlab-runner-autoregister/badges/main/pipeline.svg)
![Docker](https://img.shields.io/badge/Docker-20-9cf)
![Docker](https://img.shields.io/badge/Docker-19-9cf)

[TOC]

# GitLab runner autoregister

Automatic register / unregister capabilities and runner concurrency configuration on startup.

Based on:
- [GitLab runner](https://hub.docker.com/r/gitlab/gitlab-runner)

Source code can be found [here](https://gitlab.com/vdshop/public/docker-images/gitlab-runner-autoregister/-/tree/main).
